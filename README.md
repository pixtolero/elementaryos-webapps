# Créer des Web Apps avec les profiles Firefox

## Profiles Firefox

Dans firefox ouvrir **about:profiles** ensuite on crée un profile, par exemple nommer **youtube**

On accès au répertoire racine et on créer un dossier nommé **chrome**. Dans le dossier chrome, copier le fichier userChrome.css du dépôt. Ce fichier contient les paramètres d'affichage de la fenêtre Firefox du profile crée précédemment.

## Raccourcis et icônes

Dans les dossiers **/home/user/.local/share/applications/** et **/home/user/.local/share/icons** se trouvent respectivemet les raccourcis et les icônes pour ces derniers.

Vous pouvez copier depuis ce dépôt le fichier youtube.desktop dans le dossier **applications** et le fichier youtube.png dans **icons**.

Bien sûr vous pouvez ajuster le fichier raccourci selon vos besoins et prendre une autre icône.

## Peaufinage

Dans votre menu, vous aurez par exemple un raccouci **Youtube"". Lancez-le, vous allez avoir une nouvelle fenêtre Firefox (qui utilise le nouveau profil firefox). Dans **Personnaliser** décochez **barre de titre** afin d'épurer encore plus la fenêtre.

Dans le cas de Youtube, pensez dans les préférence à indiquer youtube.com comme page d'accueil.

Vous pouvez également installer des extensions selon le besoin spécifique de votre web app. Pour youtube, vous avez l'extension Youtube Enhancer et vous pouvez carrément mettre Ublock Origin et lui importer des filtres (faites un tour sur filterlists) qui élimines les pubs, commentaires, l'autoplay...etc

## Ce dépôt

Dans ce dépôt vous trouverez des raccourcis et icônes pour faire des web apps pour Protonmail, Mastodon, instagram, netflix, Amazon, Prime Video, Youtube et Reddit.

Source: https://www.reddit.com/r/elementaryos/comments/gzv4s6/customized_elementary_os_and_created_cool_web_apps/

